﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Delegates.Maxvalueseeker;

namespace Delegates.Console
{
    public class ConsoleWorker
    {
        /// <summary>
        /// Используется для диалога с пользователем
        /// </summary>
        private UIStage stage { get; set; }

        private FileSearcher.FileSeracher fileSearcher {get;}

        public ConsoleWorker()
        {
            this.stage = UIStage.Default;

            this.fileSearcher = new FileSearcher.FileSeracher();
        }

        public void Start()
        {
            while(this.stage != UIStage.Exit)
            {
                
                //Начало диалога
                var key = GetKey("Введите \r\n1) для демонстрации работы поиска максимального элемента в коллекуции \r\n"+
                    "2) для поиска файлов в каталоге \r\n3) для выхода");
                
                if(key == ConsoleKey.D1 || key == ConsoleKey.NumPad1)
                {
                    //Демонстрация задания 1, 5
                    this.Test1();
                }
                else if(key == ConsoleKey.D2 || key == ConsoleKey.NumPad2)
                {
                    //Демонстрация задания 2 - 5
                    this.Test2();

                }
                else if(key == ConsoleKey.D3 || key == ConsoleKey.NumPad3)
                {
                    
                    //Выход
                    this.ShutDown();
                    
                    continue;
                }
                else
                {
                    GetKey("Неверный ввод. Нажмите любую клавишу, что бы продолжить", true);

                    continue;
                }

                GetKey("Нажмите любую клавишу, что бы продолжить", true);
            }
        }

        /// <summary>
        /// Демонстрация выполнения задания 1
        /// </summary>
        private void Test1()
        {
            var data = TestDataFactory.GetTestData();

            System.Console.WriteLine("Все числовые элементы коллекции");

            foreach(var e in data)
            {
                System.Console.WriteLine($"Значение {e.FloatMember}");
            }

            System.Console.WriteLine($"Максимальны элемент {data.GetMax(s => s.FloatMember).FloatMember}");
        }

        /// <summary>
        /// Демонстрирует работу события при поиске файла
        /// </summary>
        private void Test2()
        {
            fileSearcher.FileFound += FileSearcher_FileFound;

            fileSearcher.FindFiles();

            //Убрать обработчик по завершению
            fileSearcher.FileFound -= FileSearcher_FileFound;
        }

        //Обработчик события когда файл найден
        private void FileSearcher_FileFound(object sender, FileSearcher.FileSearcherEventArgs e)
        {
            System.Console.WriteLine($"Найден файл { e.fileName}");

            //Запрос на дальнейший поиск
            System.Console.WriteLine("Нажмите 2, что бы прекратить поиск");
            
            var key = GetKey();

            if (key == ConsoleKey.D2 || key == ConsoleKey.NumPad2)
            {
                //Если пользователь выбрал 2, удаляем обработчик события
                fileSearcher.FileFound -= FileSearcher_FileFound;
                
                System.Console.WriteLine("Поиск прерван");
            }

        }

        /// <summary>
        /// Красивый выход из программы
        /// </summary>
        private void ShutDown()
        {
            for(int i = 1; i <= 3; i++)
            {
                System.Console.WriteLine($"Завершение работы через {i} сек...");
                Thread.Sleep(1000);
            }

            this.stage = UIStage.Exit;
            
        }

        /// <summary>
        /// Возвращает нажатую клавишу и декорирует ввод переносом строки
        /// </summary>
        /// <returns></returns>
        private ConsoleKey GetKey(string outputMsg = "", bool clear = false)
        {
            System.Console.WriteLine(outputMsg);
            var key = System.Console.ReadKey().Key;
            System.Console.WriteLine();

            if (clear)
                System.Console.Clear();

            return key;
        }
    }

    public enum UIStage
    {
        Default = 0,
        Started = 1,
        Cenceled = 2,
        Exit = 3
    }
}
