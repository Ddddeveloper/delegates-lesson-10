﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates.FileSearcher
{
    public class FileSearcherEventArgs: EventArgs
    {
        public string fileName { get; }

        public FileSearcherEventArgs(string fileName)
        {
            this.fileName = fileName;
        }
    }
}
