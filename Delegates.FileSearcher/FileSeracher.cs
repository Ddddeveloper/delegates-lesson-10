﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Delegates.FileSearcher
{
    public class FileSeracher
    {
        public event EventHandler<FileSearcherEventArgs> FileFound;

        //Метод, вызывающие подписавшиеся обработчики
        protected virtual void OnFileFound(FileSearcherEventArgs e)
        {
            if (FileFound != null)
                FileFound(this, e);
        }

        //Рабочий метод класса
        public void FindFiles(string path = "TestPath")
        {
            var files = Directory.GetFiles(path);

            for(int i = 0; i < files.Length; i++)
            {
                if (!string.IsNullOrEmpty(files[i]))
                {
                    //Вызов обработчика события
                    var e = new FileSearcherEventArgs(files[i]);
                    this.OnFileFound(e);
                }
                else
                {
                    //Прерывания цикла, если нет подписчиков на событие
                    break;
                }
            }
            
        }

    }
}
