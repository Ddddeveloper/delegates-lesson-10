﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates.Maxvalueseeker
{
    /// <summary>
    /// Элемент коллекции тестовых данных, среди которых будет производиться поиск максимального элемента
    /// </summary>
    public class TestData : ICalculatable
    {
        public float FloatMember { get; set; }
    }
}
