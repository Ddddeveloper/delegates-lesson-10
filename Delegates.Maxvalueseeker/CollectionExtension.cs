﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates.Maxvalueseeker
{
    public static class CollecionGetMaxExctension
    {
        /// <summary>
        /// Extension function which returns max element in the collection
        /// </summary>
        /// <typeparam name="ICalculatable"></typeparam>
        /// <param name="e">Collection</param>
        /// <param name="getParameter">delegate, which transforms ICalculatable into float</param>
        /// <returns></returns>
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T: class
        {
            var buffer = -1f;
            var result = default(T);

            foreach(T elem in e)
            {
                var temp = getParameter(elem);
                
                if (temp >= buffer)
                {
                    result = elem;
                    buffer = temp;
                }
            }
            
            return result;
        }
        
    }
}
