﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates.Maxvalueseeker
{
    public static class TestDataFactory
    {
        /// <summary>
        /// Рандомная генерация тестовых данных
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ICalculatable> GetTestData()
        {
            var random = new Random();

            var result = new List<ICalculatable>();

            for(int i = 0; i < 19; i++)
            {
                var temp = new TestData
                {
                    FloatMember = (float)random.Next(1, 201)
                };

                result.Add(temp);
            }

            return result;
        }
    }
}
